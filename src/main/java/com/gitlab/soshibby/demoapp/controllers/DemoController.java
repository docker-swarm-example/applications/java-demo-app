package com.gitlab.soshibby.demoapp.controllers;

import com.gitlab.soshibby.demoapp.configuration.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.security.Principal;

@RestController
@RequestMapping("/api")
public class DemoController {

    private static final Logger log = LoggerFactory.getLogger(DemoController.class);

    private RestTemplate restTemplate;
    private ServerConfig config;
    public static final ArrayList<Double> memoryLeakList = new ArrayList<Double>(10000000);

    public DemoController(RestTemplate restTemplate, ServerConfig config) {
        this.restTemplate = restTemplate;
        this.config = config;
    }

    @RequestMapping("/ping")
    public String ping() {
        log.info("Ping");
        return "Pong";
    }

    @RequestMapping("/chaining")
    public String chaining() {
        log.info("Chaining Start.");
        ResponseEntity<String> response = restTemplate.getForEntity("http://" + config.getHost() + ":" + config.getPort() + "/api/another-chaining", String.class);
        log.info("Chaining End.");
        return "Chaining. " + response.getBody();
    }

    @RequestMapping("/another-chaining")
    public String anotherChaining() {
        log.info("Another chaining start.");
        ResponseEntity<String> okResponse = restTemplate.getForEntity("http://" + config.getHost() + ":" + config.getPort() + "/api/ok-endpoint", String.class);
        ResponseEntity<String> errorResponse = restTemplate.getForEntity("http://" + config.getHost() + ":" + config.getPort() + "/api/error-endpoint", String.class);
        log.info("Another chaining end.");
        return "Another chaining. " + okResponse.getBody() + " " + errorResponse.getBody();
    }

    @RequestMapping("/ok-endpoint")
    public String okEndpoint() {
        log.info("OK Endpoint start.");
        log.info("OK Endpoint end.");
        return "OK, endpoint.";
    }

    @RequestMapping("/error-endpoint")
    public String errorEndpoint() {
        log.info("Error Endpoint start.");
        String url = "http://google.se";

        try {
            RestTemplate restTemplate = null;
            return restTemplate.getForEntity(url, String.class).getBody();
        } catch (Exception e) {
            log.error("Failed to get " + url + ".", e);
            throw new RuntimeException("Failed to get " + url + ".", e);
        }
    }

    @RequestMapping("/memory-leak")
    public String memoryLeakEndpoint() {
        log.info("Memory Leak Endpoint Called.");

        for (double i = 0; i < 1000000; i++) {
            memoryLeakList.add(i);
        }

        return "Oops, memory leak...";
    }

}
