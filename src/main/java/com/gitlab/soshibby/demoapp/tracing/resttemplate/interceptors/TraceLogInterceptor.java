package com.gitlab.soshibby.demoapp.tracing.resttemplate.interceptors;

import com.uber.jaeger.SpanContext;
import io.opentracing.Tracer;
import io.opentracing.contrib.spring.web.client.RestTemplateSpanDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.List;

public class TraceLogInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(TraceLogInterceptor.class);
    private Tracer tracer;
    private List<RestTemplateSpanDecorator> spanDecorators;

    public TraceLogInterceptor(Tracer tracer, List<RestTemplateSpanDecorator> spanDecorators) {
        this.tracer = tracer;
        this.spanDecorators = spanDecorators;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        SpanContext context = (SpanContext) tracer.scopeManager().active().span().context();
        String traceId = String.format("%x", context.getTraceId());
        String spanId = String.format("%x", context.getSpanId());
        String parentSpanId = String.format("%x", context.getParentId());

        MDC.put("traceId", traceId);
        MDC.put("spanId", spanId);
        MDC.put("parentSpanId", parentSpanId);

        log.info("Sending request to: " + request.getURI());

        ClientHttpResponse response;

        response = execution.execute(request, body);

        if (response.getStatusCode().is5xxServerError()) {
            tracer.activeSpan().setTag("error", true);
        }

        log.info("Received response: " + response.getStatusCode() + " from " + request.getURI());

        MDC.remove("traceId");
        MDC.remove("spanId");
        MDC.remove("parentSpanId");
        return response;
    }

}
