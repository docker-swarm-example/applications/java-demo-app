FROM gradle:jdk10 as builder

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:10-jre-slim
EXPOSE 8080
COPY --from=builder /home/gradle/src/build/libs/java-demo-app-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app
CMD java -jar java-demo-app-0.0.1-SNAPSHOT.jar

